"""
Librairies de ML
"""
import tensorflow as tf
import pandas as pd
import numpy as np

from tensorflow.contrib import rnn
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score, accuracy_score, recall_score, precision_score


"""
Keras specific imports
"""
import keras
from keras.models import Sequential, load_model
from keras.layers import Dense, Activation, Embedding, Dropout, TimeDistributed
from keras.layers import LSTM
from keras.callbacks import CSVLogger
from keras.callbacks import Callback
from keras.optimizers import Adam
from keras.utils import to_categorical
from keras.callbacks import ModelCheckpoint
from keras.models import load_model


"""
Autres Librairies
"""
import time
import json
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

starting = time.time()
"""
Constantes
"""
HOUR = 60
DAY = 1440
WEEK = 10080
MONTH = 43800
MONTH3 = 131400
YEAR = 525600
DATASET_FILENAMES = [#'final_data0.csv',
                     'final_data1.csv',
                     'final_data2.csv',
                     'final_data3.csv',
                     'final_data4.csv',
                     #'final_data5.csv', #  Test Set
                     'final_data6.csv',
                     'final_data7.csv',
                     ]

#tf.enable_eager_execution();
# Testing Tensorflow is working
#print(tf.reduce_sum(tf.random_normal([1000, 1000])))

# Setting up the hyperparameters
epochs = 10
n_classes = 1
n_units = 200
n_features = 29
batch_size = 1024
lstm_out = 64
filename = "L64L64D32-d"
#filename = "D256-128-1-a"
dataset_name = "final_data5.csv"
n_features = 11
debut = time.time()
n_dataset = 6

# And removing the unwanted one

# Setting up the data (with lookbacks of week, and month)
#features = create_dataset(features)
#X_train,X_test,y_train,y_test = train_test_split(features, labels, test_size=0.2, shuffle=False, random_state=42)

# dataset = np.concatenate((features, labels), axis=1)
# Remove unwanted labels
# dataset = dataset[:, :-2]



#X_train = np.reshape(X_train, (X_train.shape[0], 1, 1))
#y_train = np.reshape(y_train, (y_train.shape[0], 1, 1))

embed_dim = 128

# Printing the time to load the varrror whrror when checking target: expected dense_2 to have shape (64,) but got array with shape (1,)en checking target: expected dense_2 to have shape (64,) but got array with shape (1,)
print("Time to load vars: ", time.time() - starting)

def build_graph_dense():
    """ Construit et retourn un simple NN dense, pour comparaison avec LSTM

    Returns
    -------
    model : le graphe du modele a 256 x 128 x 1
    """

    model = Sequential()
    model.add(Dense(256, input_dim=66, activation='relu'))
    model.add(Dense(128, activation='relu'))
    model.add(Dense(1, activation='linear'))
    model.compile(loss='mean_squared_error',
                  optimizer='adam',
                  metrics=['mean_squared_error',
                           'mean_absolute_error',
                           'mean_absolute_percentage_error'])
    print("Temps de compilation : ", time.time() - debut)
    print(model.summary())
    return model

def build_graph_lstm():
    """ Construit et retourne le graphe du Neural Net
    Fonction imprime egalement le temps pris pour compiler le graphe ainsi que celui-ci

    Returns
    -------
    model : le model compile et pret a etre entrainer
    """

    model = Sequential()

    #model.add(Dense(64, inpdataset = np.insert(features, features.shape[1], labels[:, 1], axis=1)ut_shape=(12,)))
    model.add(LSTM(input_shape=(77, 1,),
                   units=lstm_out,
                   recurrent_dropout=0.2,
                   dropout=0.2,
                   return_sequences=True))

    model.add(LSTM(units=lstm_out,
                   recurrent_dropout=0.2,
                   dropout=0.2,
                   return_sequences=False,
                   stateful=False))

    #model.add(Dense(input_shape=(1,batch_size), units=32))
    model.add(Dense(64))
    model.add(Dense(1, activation='linear'))

    debut = time.time()
    model.compile(loss = 'mean_squared_error',
                  optimizer='adam',
                  metrics=['mean_squared_error',
                            'mean_absolute_percentage_error'])

    print("Temps de compilation : + str(debut-time.time()", time.time() - debut)
    print(model.summary())

    return model

def load_dataset(dataset_name):
    """
    Instead of creating the whole dataset, just load it from disk (after having
    run create_dataset at least once

    Parameters
    ----------
    filename : Name of the file to load from

    Returns
    -------
    dataset : the 2D dataset
    """
    print("\nCreating dataset |==>-----| (loading from file", dataset_name, " to Dataframe)" )
    dataset = pd.read_csv(dataset_name, skiprows=[1], header=None)
    print("Creating dataset |===>----| (transforming into numpy array)")
    dataset = dataset.to_numpy()
    return dataset

def create_dataset(seq_len):
    """
    Takes the raw data (2D) and turns it into a 3D tensor of depth seq_len

    Parameter
    ---------
    dataset : 2D numpy array - the data to transform
    seq_len : the length of the sequence to feed in the lstm

    Return
    ------
    dataset : The transformed dataset
    """
    # Getting the data
    features = pd.read_csv('processed_data.csv', skiprows=[1], header=None)
    labels = pd.read_csv('labels.csv', skiprows=[1], header=None)
    upper_bound =  features.shape[0] # int(data.shape[0]/4)

    # Selecting wanted columns
    features = features.drop(columns=6)
    features = features.iloc[1:upper_bound, 1:12]
    labels = labels.iloc[1:upper_bound, :] # Predicting the 1hr difference (1st column in labels)
    seq_len += 1

    print("Creating dataset |==>-----| (df to numpy array)")
    features = features.to_numpy()
    labels = labels.to_numpy()

    # Concatenate labels and features (to ease processing) (add a column)
    dataset = np.insert(features, features.shape[1], labels[:, 1], axis=1)

    # Ajouter les prix de la derniere annee

    n_features = dataset.shape[1] -1

    print("Creating dataset |====>---| (Adding dimensions)")
    features =  np.zeros((dataset.shape[0], n_features * 6))
    print("Dataset :", dataset.shape, " and features: ", features.shape)
    for x in range(dataset.shape[0]):
        if x - HOUR > 0:
            features[x, 0:n_features] = dataset[x-HOUR, :-1]
        if x - DAY > 0:
            features[x, n_features:2*n_features] = dataset[x-DAY, :-1]
        if x - (2 * DAY) > 0:
            features[x, 2*n_features:3*n_features] = dataset[x-(2*DAY), :-1]
        if x - WEEK > 0:
            features[x, 3*n_features:4*n_features] = dataset[x-(WEEK), :-1]
        if x - (2 * WEEK) > 0:
            features[x, 4*n_features:5*n_features] = dataset[x-(2*WEEK), :-1]
        if x - (MONTH) > 0:
            features[x, 5*n_features:6*n_features] = dataset[x-(MONTH), :-1]

    print("Creating dataset |=====>--| (inserting new data into old)")
    dataset = np.insert(dataset, dataset.shape[1] - 1 , np.transpose(features[:, :]), axis=1)

    #print("Length : ", dataset[1], seq_len)
    #processed_data = np.zeros((dataset.shape[0] - seq_len + 1, dataset.shape[1], seq_len))
    #for x in range(0, dataset.shape[0] - seq_len):
    #    processed_data[0] = np.transpose(dataset[x:x+seq_len])

    # This is for the concatenation of different moments (eg. last month data)
    # processed_data =  np.array((features.shape[0], features.shape[1] * 5), seq_len)
    # for i in range(1, len(features)):
    #   processed_data[i, ]
    #for x in range(len(dataset) - seq_len):
    #   processed_data.append((dataset[x: x + seq_len]))


    # Export the dataset created for future use
    print("Creating dataset |======>-| (printing to file part 0)")
    for i in range(7):
        slice = int(dataset.shape[0] /7) -1
        print("Creating dataset |======>-| (printing to file part ", i, ")")
        pd.DataFrame(dataset[slice*i: slice*i + slice, :]).to_csv("final_data"+str(i)+".csv", index=None, header=False)

    return dataset


def split_test_train(dataset):
    features = dataset[:, :-1, :]
    labels = dataset[:, -1, :]
    x_train, x_test, y_train, y_test = train_test_split(
        features, labels, test_size=0.2, shuffle=False, random_state=42)
    return x_train, x_test, y_train, y_test


class TestCallback(Callback):
    def __init__(self, test_data):
        self.test_data = test_data

    def on_epoch_end(self, epoch, logs={}):
        x, y = self.test_data
        loss, acc = self.model.evaluate(x, y, verbose=1)
        print('\nTesting loss: {}, acc: {}\n'.format(loss, acc))


class Batch_Generator(keras.utils.Sequence):

    def __init__(self, dataset_filenames, dataset_len, batch_size):
        self.dataset_filenames = dataset_filenames
        self.dataset_len = dataset_len
        self.batch_size = batch_size
        self.n_set = 0
        self.array = load_dataset(dataset_filenames[0])
        self.current_index = 0
        self.array_lower = 0
        self.array_first_id = 0

    def __len__(self):
        return int(np.floor(self.dataset_len / self.batch_size))

    def __getitem__(self, idx):
        print("IDX = ", idx)

        # At each epoch, reset variables
        if idx <= 1:
            self.n_set = 0
            self.array = load_dataset(self.dataset_filenames[0])
            self.current_index = 0
            self.array_lower = 0
            self.array_first_id = 0

        self.current_index = idx * self.batch_size
        lower = self.current_index - self.array_first_id #+ (idx * self.batch_size)
        upper = self.current_index - self.array_first_id + self.batch_size #+ ((idx + 1) * self.batch_size)

        if lower < 0:
            lower = 0
        # Si tout les data sont deja loader
        if upper <= self.array.shape[0]:
            batch_x = self.array[lower:upper, :-1]
            batch_y = self.array[lower:upper, -1]
            self.current_index += self.batch_size
        # Si jai seulemeent un partie de la batch de pre loader
        else:
            i = 0
            batch_x = np.zeros((batch_size, self.array.shape[1]-1))
            batch_y = np.zeros((batch_size,))
            # Tant que je suis dans le dataset loader, ajouter
            while i < batch_size:
                if lower + i < self.array.shape[0]:
                    batch_x[i] = self.array[lower + i, :-1]
                    batch_y[i] = self.array[lower + i, -1]
                    self.current_index += 1
                    i += 1
                # Puis loader le prochain dataset
                else:
                    # S<il reste des dataset
                    if self.n_set < len(self.dataset_filenames) - 1:
                        self.n_set += 1
                        self.array_first_id = self.current_index
                    # Si je les ai tous deja parcourus
                    else:
                        self.n_set = 0
                        self.current_index = 0
                        self.array_first_id  = 0

                    # Loader le prochain set, rectifier lower, ajouter a batch et iterer le i
                    self.array = load_dataset(self.dataset_filenames[self.n_set])
                    lower = -i
                    batch_x[i] = self.array[lower + i, :-1]
                    batch_y[i] = self.array[lower + i, -1]
                    self.current_index += 1
                    i +=1


        #batch_x = self.dataset_filenames[idx * self.batch_size: (idx + 1) * self.batch_size]
        #batch_y = self.labels[idx * self.batch_size: (idx + 1) * self.batch_size]
        batch_x = np.reshape(batch_x, (batch_x.shape[0], batch_x.shape[1], 1))

        return np.array(batch_x), np.array(batch_y)

    def load_next(dataset_filenames, n_set):
        return load_dataset(dataset_filenames[n_set])


def plot_graph(history):
    T = 0.001
    history_val_loss = []

    for x in history.history['val_loss']:
        if x >= T:
            history_val_loss.append(T)
        else:
            history_val_loss.append(x)

    plt.figure(6)
    plt.plot(history.history['loss'])
    plt.plot(history_val_loss)
    plt.title('model loss adjusted')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig('models/'+ filename + '.png', format='png')


"""

Do the actual work     
Loading, cleaning, preapring the data

"""


# Cleaning up variables not to get Memory overflow
features = None
"""
This part is for loading the whole dataset at once
It is rendered obsolete by the Batch_Generator Class

"""

# Add the lookbacks (HOUR, DAY, DAY*2, WEEK, WEEK*2, MONTH)
print("Creating dataset |==>-----|")
#dataset = create_dataset(10)

dataset = load_dataset(dataset_name)

print("Creating dataset |========| (splitting train test)")
x_train,x_test,y_train,y_test = train_test_split(dataset[:, :-1], dataset[:, -1],
                                                 test_size=0.1, shuffle=False, random_state=42)
# Reshaping the tensors
x_train = np.reshape(x_train, (len(x_train), len(x_train[0]), 1))
x_test = np.reshape(x_test, (len(x_test), len(x_test[0]), 1))
#x_train, x_test, y_train, y_test = split_test_train(dataset)


# For logging details
csv_logger = CSVLogger('models/training-' + filename + '.log', append=True)
callback = TestCallback((x_test, y_test))

# Cleaning up variables not to get Memory overflow
dataset = load_dataset(DATASET_FILENAMES[0])
dataset_len = (dataset.shape[0] / n_dataset) -8

"""

Building and running the model

"""

training_batch_generator = Batch_Generator(DATASET_FILENAMES, dataset_len, batch_size)
model = build_graph_lstm()

# Only for Dense Model
# model = build_graph_dense()
# x_train = x_train.flatten(x_train)
# x_test = x_test.flatten(x_train)

hist = model.fit_generator(
    generator=training_batch_generator,
    #x_train,
    #y_train,
    #batch_size=batch_size,
    steps_per_epoch=  int(dataset_len / batch_size),
    epochs=epochs,
    shuffle=False,
    # validation_split=0.1,
    callbacks=[csv_logger])

model.evaluate(x_test, y_test, batch_size)

plot_graph(hist)
# Saving the model once training is done
model.save('models/' + filename + '.h5')
json_string = model.to_json()
with open('models/' + filename + '.json', 'w') as json_file:
    json.dump(json_string, json_file)


