"""
Librairies de ML
"""
import tensorflow as tf
import pandas as pd
import numpy as np

from tensorflow.contrib import rnn
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score, accuracy_score, recall_score, precision_score


"""
Keras specific imports
"""
from keras.models import Sequential, load_model
from keras.layers import Dense, Activation, Embedding, Dropout, TimeDistributed
from keras.layers import LSTM
from keras.callbacks import CSVLogger
from keras.callbacks import Callback
from keras.optimizers import Adam
from keras.utils import to_categorical
from keras.callbacks import ModelCheckpoint
from keras.models import load_model


"""
Autres Librairies
"""
import time
import json

starting = time.time()
"""
Constantes
"""

HOUR = 60
DAY = 1440
WEEK = 10080
MONTH = 43800
MONTH3 = 131400
YEAR = 525600


#tf.enable_eager_execution();
# Testing Tensorflow is working
#print(tf.reduce_sum(tf.random_normal([1000, 1000])))

# Setting up the hyperparameters
epochs = 10
n_classes = 1
n_units = 200
n_features = 29
batch_size = 512
lstm_out = 64
#filename = "L64L64D32-a"
filename = "D256-128-1-a"
dataset_name = "final_data5.csv"
n_features = 11
debut = time.time()

# And removing the unwanted one

# Setting up the data (with lookbacks of week, and month)
#features = create_dataset(features)
#X_train,X_test,y_train,y_test = train_test_split(features, labels, test_size=0.2, shuffle=False, random_state=42)

# dataset = np.concatenate((features, labels), axis=1)
# Remove unwanted labels
# dataset = dataset[:, :-2]



#X_train = np.reshape(X_train, (X_train.shape[0], 1, 1))
#y_train = np.reshape(y_train, (y_train.shape[0], 1, 1))

embed_dim = 128

# Printing the time to load the varrror whrror when checking target: expected dense_2 to have shape (64,) but got array with shape (1,)en checking target: expected dense_2 to have shape (64,) but got array with shape (1,)
print("Time to load vars: ", time.time() - starting)

def build_graph_dense():
    """ Construit et retourn un simple NN dense, pour comparaison avec LSTM

    Returns
    -------
    model : le graphe du modele a 256 x 128 x 1
    """

    model = Sequential()
    model.add(Dense(256, input_dim=66, activation='relu'))
    model.add(Dense(128, activation='relu'))
    model.add(Dense(1, activation='linear'))
    model.compile(loss = 'mean_squared_error', optimizer='adam',metrics=['mean_squared_error',
                                                                         'mean_absolute_error',
                                                                         'mean_absolute_percentage_error'])
    print("Temps de compilation : ", time.time() - debut)
    print(model.summary())
    return  model

def load_dataset(dataset_name):
    """
    Instead of creating the whole dataset, just load it from disk (after having
    run create_dataset at least once

    Parameters
    ----------
    filename : Name of the file to load from

    Returns
    -------
    dataset : the 2D dataset
    """
    print("Creating dataset |==>-----| (loading from file to Dataframe)" + str(debut-time.time()))
    dataset = pd.read_csv(dataset_name, skiprows=[1], header=None)
    print("Creating dataset |===>----| (transforming into numpy array)"+ str(debut-time.time()))
    dataset = dataset.to_numpy()
    return dataset

def create_dataset(seq_len):
    """
    Takes the raw data (2D) and turns it into a 3D tensor of depth seq_len

    Parameter
    ---------
    dataset : 2D numpy array - the data to transform
    seq_len : the length of the sequence to feed in the lstm

    Return
    ------
    dataset : The transformed dataset
    """
    # Getting the data
    features = pd.read_csv('processed_data.csv', skiprows=[1], header=None)
    labels = pd.read_csv('labels.csv', skiprows=[1], header=None)
    upper_bound =  features.shape[0] # int(data.shape[0]/4)

    # Selecting wanted columns
    features = features.drop(columns=6)
    features = features.iloc[1:upper_bound, 1:12]
    labels = labels.iloc[1:upper_bound, :]            # Predicting the 1hr difference (1st column in labels)
    seq_len += 1

    print("Creating dataset |==>-----| (df to numpy array)")
    features = features.to_numpy()
    labels = labels.to_numpy()

    # Concatenate labels and features (to ease processing) (add a column)
    dataset = np.insert(features, features.shape[1], labels[:, 1], axis=1)

    # Ajouter les prix de la derniere annee

    n_features = dataset.shape[1] -1

    print("Creating dataset |====>---| (Adding dimensions)")
    features =  np.zeros((dataset.shape[0], n_features * 6))
    print("Dataset :", dataset.shape, " and features: ", features.shape)
    for x in range(dataset.shape[0]):
        if x - HOUR > 0:
            features[x, 0:n_features] = dataset[x-HOUR, :-1]
        if x - DAY > 0:
            features[x, n_features:2*n_features] = dataset[x-DAY, :-1]
        if x - (2 * DAY) > 0:
            features[x, 2*n_features:3*n_features] = dataset[x-(2*DAY), :-1]
        if x - WEEK > 0:
            features[x, 3*n_features:4*n_features] = dataset[x-(WEEK), :-1]
        if x - (2 * WEEK) > 0:
            features[x, 4*n_features:5*n_features] = dataset[x-(2*WEEK), :-1]
        if x - (MONTH) > 0:
            features[x, 5*n_features:6*n_features] = dataset[x-(MONTH), :-1]


    print("Creating dataset |=====>--| (inserting new data into old)")
    dataset = np.insert(dataset, dataset.shape[1] - 1 , np.transpose(features[:, :]), axis=1)


    #print("Length : ", dataset[1], seq_len)
    #processed_data = np.zeros((dataset.shape[0] - seq_len + 1, dataset.shape[1], seq_len))
    #for x in range(0, dataset.shape[0] - seq_len):
    #    processed_data[0] = np.transpose(dataset[x:x+seq_len])



    # This is for the concatenation of different moments (eg. last month data)
    # processed_data =  np.array((features.shape[0], features.shape[1] * 5), seq_len)
    # for i in range(1, len(features)):
    #   processed_data[i, ]
    #for x in range(len(dataset) - seq_len):
    #   processed_data.append((dataset[x: x + seq_len]))


    # Export the dataset created for future use
    print("Creating dataset |======>-| (printing to file part 0)")
    for i in range(7):
        slice = int(dataset.shape[0] /7) -1
        print("Creating dataset |======>-| (printing to file part ", i, ")")
        pd.DataFrame(dataset[slice*i: slice*i + slice, :]).to_csv("final_data"+str(i)+".csv", index=None, header=False)

    return dataset


def split_test_train(dataset):
    features = dataset[:, :-1, :]
    labels = dataset[:, -1, :]
    x_train,x_test,y_train,y_test = train_test_split(features, labels, test_size=0.2, shuffle=False, random_state=42)
    return x_train, x_test, y_train, y_test


class TestCallback(Callback):
    def __init__(self, test_data):
        self.test_data = test_data

    def on_epoch_end(self, epoch, logs={}):
        x, y = self.test_data
        loss, acc = self.model.evaluate(x, y, verbose=1)
        print('\nTesting loss: {}, acc: {}\n'.format(loss, acc))


"""
Do the actual work     
"""
#def main(dataset):


# Cleaning up variables not to get Memory overflow
features = None

# Add the lookbacks (HOUR, DAY, DAY*2, WEEK, WEEK*2, MONTH)
print("Creating dataset |==>-----|")
#dataset = create_dataset(10)

dataset = load_dataset(dataset_name)

print("Creating dataset |========| (splitting train test)")
x_train,x_test,y_train,y_test = train_test_split(dataset[:, :-1], dataset[:, -1],
                                                 test_size=0.1, shuffle=False, random_state=42)
# Reshaping the tensors
#x_train = np.reshape(x_train, (len(x_train), len(x_train[0]), 1))
#x_test = np.reshape(x_test, (len(x_test), len(x_test[0]), 1))
#x_train, x_test, y_train, y_test = split_test_train(dataset)

# For logging details
csv_logger = CSVLogger('models/training-' + filename + '.log')
callback = TestCallback((x_test, y_test))

# Cleaning up variables not to get Memory overflow
dataset = None

model = build_graph_dense()

# only for Dense Model
#x_train = x_train.flatten(x_train)
#x_test = x_test.flatten(x_train)

model.fit(
    x_train,
    y_train,
    batch_size=batch_size,
    epochs=epochs,
    validation_split=0.1,
    callbacks=[csv_logger])

model.evaluate(x_test, y_test, batch_size)

# Printing the whole time
print("Time to train vacopyrs: ", time.time() - starting)
model.save('models/' + filename + '.h5')
json_string = model.to_json()
with open('models/' + filename + '.json', 'w') as json_file:
    json.dump(json_string, json_file)
#main(dataset)
