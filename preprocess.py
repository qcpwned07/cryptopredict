import pandas as pd
import numpy as np
import sklearn as sk
from sklearn.preprocessing import MinMaxScaler


# 60 minutes in 1 hour, DAY in day, WEEK in week, MONTH in month YEAR in year
HOUR = 60
DAY = 1440
WEEK = 10080
MONTH = 43800
MONTH3 = 131400
YEAR = 525600

raw_data = np.array()
processed_data = np.array()
rows = 0
cols = 0

def load_data ():
    #Importing the data into df
    raw_data = pd.read_csv("bistamp_1min_data.csv",  skiprows = 0)
    raw_data2 = pd.read_csv("raw_dataset.csv",  skiprows = 0)
    processed_data2 = pd.read_csv("processed_data.csv",  skiprows = 0)

    # Double checking the data
    print(raw_data.head(3))
    print(raw_data.shape)

    #Transfering to np array
    raw_data = raw_data.to_numpy()
    raw_data2 = raw_data2.to_numpy()
    processed_data2 = processed_data2.to_numpy()

    rows = raw_data.shape[0]
    cols = raw_data.shape[1]

    append_array = np.zeros((rows, cols+6))
    append_array[:, :-6] = raw_data

    labels = np.zeros((rows,4))
    raw_data = append_array

    processed_data = np.zeros((rows, cols + 6))

# Creating the y dataset, prediction for an hour go up to rows - 60,
#       prediction of day go up to rows - DAY
#       and prediction of week go up to rows - Week
#       and prediction of Month go up to rows - Week
def create_labels():
    raw_data = raw_data2
    #processed_data2 = scaler.fit_transform(processed_data2)
    for x in range(0,rows-1):
        labels[x, 0] = (raw_data[x + 1, 7] - raw_data[x, 7]) / raw_data[x, 7]
    for x in range(0,rows - int(1.5 * HOUR)):
        labels[x, 1] = (raw_data[x + int(1.5 * HOUR), 8] - raw_data[x, 7]) / raw_data[x, 7]
    for x in range(0,rows - int(1.5 * DAY)):
        labels[x, 2] = (raw_data[x + int(1.5 * DAY), 9] - raw_data[x, 7]) / raw_data[x, 7]
    for x in range(0,rows - int(1.5 * WEEK)):
        labels[x, 3] = (raw_data[x + int(1.5 * WEEK), 10] - raw_data[x, 7]) / raw_data[x, 7]

def fill_NaN_fields():
    # Filling in missing values with last avail. value
    for x in range(0, rows):
        for y in range(0, cols):
            if np.isnan(raw_data[x, y]):
                if y == 5 or y == 6:
                    raw_data[x,y] = 0
                else:
                    raw_data[x, y] = raw_data[x-1, y]


def change_data_to_delta():
    for x in range(1, rows):
        processed_data[x, 1] = raw_data[x, 1]
        for y in range(0, cols):
            # Fifth column (volume
            if y==0 or y == 5 or y == 6:
                processed_data[x, y] = raw_data[x,y]
            elif y < 8:
                processed_data[x, y] = (raw_data[x, y] - raw_data[x-1, y]) / raw_data[x, y]

        # Processing average values into changes
        y = 8
        offset = HOUR
        if x > HOUR:
            processed_data[x, y] = (raw_data[x, y] - raw_data[x-offset, y]) / raw_data[x, y]

        y = 9
        offset = DAY
        if x > DAY:
            processed_data[x, y] = (raw_data[x, y] - raw_data[x-offset, y]) / raw_data[x, y]

        y = 10
        offset = WEEK
        if x > WEEK:
            processed_data[x, y] = (raw_data[x, y] - raw_data[x-offset, y]) / raw_data[x, y]

        y = 11
        offset = MONTH
        if x > MONTH:
            processed_data[x, y] = (raw_data[x, y] - raw_data[x-offset, y]) / raw_data[x, y]

        y = 12
        offset = MONTH3
        if x > MONTH3:
            processed_data[x, y] = (raw_data[x, y] - raw_data[x-offset, y]) / raw_data[x, y]

        y = 13
        offset = YEAR
        if x > YEAR:
            processed_data[x, y] = (raw_data[x, y] - raw_data[x-offset, y]) / raw_data[x, y]

        if x % 1000000 == 0:
            print (x)


def add_averages():
    # Y weighted avg
    y = 7
    hour_total = 0
    day_total = 0
    week_total = 0
    month_total = 0
    month3_total = 0
    year_total = 0

    # 60 minutes in 1 hour, DAY in day, WEEK in week, MONTH in month YEAR in year

    # Adding the firsts 60 minutes
    for i in range(0, HOUR):
        hour_total += raw_data[i, y]
    for x in range(HOUR, rows):
        # Shifting the hourly total
        hour_total -= raw_data[x-60, y]
        hour_total += raw_data[x, y]
        # Taking  the average and putting it into the table
        hour_avg = hour_total / 60
        raw_data[x, y+1] = hour_avg

    # Finding for DAY
    for i in range(0, DAY):
        day_total += raw_data[i, y]
    for x in range(DAY, rows):
        day_total -= raw_data[x-DAY, y]
        day_total += raw_data[x, y]
        day_avg = day_total / DAY
        raw_data[x, y+2] = day_avg

    # Computing for WEEK
    for i in range (0, WEEK):
        week_total += raw_data[i, y]
    for x in range(WEEK, rows):
        week_total -= raw_data[x-WEEK, y]
        week_total += raw_data[x, y]
        week_avg = week_total / WEEK
        raw_data[x, y+3] = week_avg

    for i in range(0, MONTH):
        month_total += raw_data[i, y]
    for x in range(MONTH, rows):
        month_total -= raw_data[x-MONTH, y]
        month_total += raw_data[x, y]
        month_avg = month_total / MONTH
        raw_data[x, y+4] = month_avg

    # Computing for 3MONTH
    for i in range(0, MONTH3):
        month3_total += raw_data[i, y]
    for x in range(MONTH3, rows):
        month3_total -= raw_data[x-MONTH3, y]
        month3_total    += raw_data[x, y]
        month3_avg = month3_total / MONTH3
        raw_data[x, y+5] = month3_avg

    # Computing for YEARS
    for i in range(0, YEAR):
        year_total += raw_data[i, y]
    for x in range(YEAR, rows):
        year_total -= raw_data[x-YEAR, y]
        year_total += raw_data[x, y]
        year_avg = year_total / YEAR
        raw_data[x, y+6] = year_avg




# Do the actual work
def process_data_and_export():
    fill_NaN_fields()
    add_averages()
    change_data_to_delta()

    # Put all in dataframe
    columns = ["timestamp", "open",
               "high", "low", "close",
               "volumeBTC",
               "volumeCufrrency",
               "weigthedPrice", "hour",
               "day", "week",
               "month", "3month", "year"]

    dataset = pd.DataFrame(data=processed_data, columns=columns)
    raw_dataset = pd.DataFrame(data=raw_data, columns=columns)
    export_csv = dataset.to_csv('processed_data.csv', index = None, header=True,)
    export_csv = raw_dataset.to_csv('raw_dataset.csv', index = None, header=True)

    print(processed_data.shape)
    print(processed_data[669031:669039])
    print("Finalizing..... Done")


def process_labels_and_export():
    create_labels()
    columns = ["minute", "hourly", "daily", "monthly"]
    dataset = pd.DataFrame(data=labels, columns=columns)
    export_csv = dataset.to_csv ('labels.csv', index = None, header=True,)

#process_labels_and_export()

scaler = MinMaxScaler(feature_range=(0, 1))

#processed_data2 = scaler.fit_transform(processed_data2)
