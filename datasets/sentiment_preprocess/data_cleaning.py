import glob
import os

MAX_FILE_LEN = 3e8

POST_COLS = {
    'THREAD_ID': 0,
    'POST_ID':   1,
    'AUTHOR_ID': 2,
    'DATE':      3,
    'CONTENT':   4
}

def remove_duplicate_entries():
    filenames = sorted(glob.glob('E:/data_bitcointalk/*.txt'))
    for fn in filenames:
        entries = ""
        with open(fn, "r", encoding="utf-8") as fp:
            entries = fp.read()
        entries = entries.split(";;\n")
        entries = list(set(entries))
        with open(fn, "w", encoding="utf-8") as fp:
            fp.write(";;\n".join(entries))

def concat_files(board):
    filenames = sorted(glob.glob('E:/data_bitcointalk/board{}_posts_*.txt'.format(board)))
    filenames = [(filenames[i], filenames[i+1]) for i in range(0,len(filenames)-1,2)]
    for (fn1, fn2) in filenames:
        size1 = os.stat(fn1).st_size
        size2 = os.stat(fn2).st_size
        if size1 + size2 <= MAX_FILE_LEN:
            content = ""
            with open(fn2, "r", encoding="utf-8") as fp:
                content = fp.read()
            with open(fn1, "a", encoding="utf-8") as fp:
                fp.write(content)
            with open(fn2, "w", encoding="utf-8") as fp:
                fp.write("")

def remove_empty_entries():
    filenames = sorted(glob.glob('E:/data_bitcointalk/board*_posts_*.txt'))
    for fn in filenames:
        fn2 = "{}EMPTY_board{}".format(fn.split('board')[0],fn.split('board')[1])
        entries = ""
        with open(fn, "r", encoding="utf-8") as fp:
            entries = fp.read()
        #empties = entries.count("\t;;")
        entries = entries.split(";;\n")
        entries = [e.split(";\t") for e in entries if len(e) > 0]
        #len1 = len(entries)

        nonentries = [";\t".join(e) for e in entries if e[POST_COLS['CONTENT']] == ""]
        with open(fn2, "w", encoding="utf-8") as fp:
            fp.write(";;\n".join(nonentries))

        entries = [";\t".join(e) for e in entries if e[POST_COLS['CONTENT']] != ""]
        #len2 = len(entries)
        #test = (len1 - empties == len2)
        #print("{}:\n\t{} --> {}. (-{}) <{}>\n".format(fn, len1, len2, empties, test))
        with open(fn, "w", encoding="utf-8") as fp:
            fp.write(";;\n".join(entries))