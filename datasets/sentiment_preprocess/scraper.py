# https://docs.python-guide.org/scenarios/scrape/
# https://lxml.de/lxmlhtml.html
from lxml import html, etree
import requests
from datetime import datetime
import re
import glob
import os
import time

DATE_MIN = datetime.strptime('01-01-2012', '%d-%m-%Y')
DATE_MAX = datetime.strptime('14-03-2019', '%d-%m-%Y')
TODAY = "{} {}, {},".format(datetime.now().strftime("%B"), datetime.today().day, datetime.today().year)
PATH = "F:/data_bitcointalk"

BOARDS = [1, 57, 67, 224]
# 1 = Bitcoin Discussion OK
# 57 = Economy > Speculation OK
# 67 = Altcoin Discussion OK
# 224 = Speculation (Altcoins)

CONST = {
    "POSTS_PER_PAGE": 20,
    "THREADS_PER_PAGE": 40,
}

# list actual board to scrape
# file output
# log progress

def getPage(i, base_url, items_per_page=0):
    url = base_url[:-1] + str(items_per_page*i)
    time.sleep(0.8) # ]0.6, 0.7]
    return requests.get(url)

def getPageCount(url):
    page = getPage(0, url)
    tree = html.fromstring(page.content)
    navPages = tree.find_class("navPages")
    return 1 if len(navPages) == 0 else int(navPages[-2].text_content())


# ==============================================================================

class Post:
    def __init__(self, posterinfo_tree=None, post_tree=None):
        self.post_id = None
        self.thread_id = None
        self.author_id = self._find_author_id(posterinfo_tree)
        self.post_date, self.post_content = self._find_post(post_tree)
        while '\t' in self.post_content:
            self.post_content = self.post_content.replace('\t',' ')
        while ';' in self.post_content:
            self.post_content = self.post_content.replace(';',':')
        
    def _find_author_id(self, posterinfo_tree=None):
        posterinfo = html.tostring(posterinfo_tree).decode("utf-8")
        author_id = posterinfo[len("""<td valign="top" width="16%" rowspan="2" style="overflow: hidden;" class="poster_info">
       <b><a href="https://bitcointalk.org/index.php?action=profile;u="""):]
        author_id = author_id[:author_id.find('"')]
        return author_id
    
    def _find_post(self, post_tree=None):
        post = post_tree.find_class("post")[0]
        text_content = html.tostring(post).decode("utf-8")
        
        # Remove quotes
        if '<div class="quoteheader">' in text_content:
            quoteheaders = post_tree.find_class("quoteheader")
            quotes = post_tree.find_class("quote")
            for i in range(min(len(quoteheaders), len(quotes))):
                qh_text = html.tostring(quoteheaders[i]).decode("utf-8")
                q_text = html.tostring(quotes[i]).decode("utf-8")
                if qh_text in text_content:
                    fst = text_content.find(qh_text)
                    lst = fst + len(qh_text) + len(q_text)
                    text_content = text_content[:fst] + text_content[lst:]

        clean_content = text_content[len('<div class="post">'):-2*len("</div>")-1
           ].replace("<br>","\n"
           ).replace("&#133;","..."
           ).replace("&#146;","'"
           ).replace("&#147;",'"'
           ).replace("&#148;",'"'
           ).replace("&#160;"," "
           )

        # Remove smileys
        for fst in [m.start() for m in re.finditer('<img src="https://bitcointalk.org/Smileys', clean_content)][::-1]:
            lst = clean_content.find('>', fst+1) + len(">")
            alt_fst = clean_content.find('alt="', fst+1) + len('alt="')
            alt_lst = clean_content.find('"', alt_fst+1)
            alt = "*" + clean_content[alt_fst:alt_lst] + "*"
            clean_content = clean_content[:fst] + alt + clean_content[lst:]
        
        smalltext = post_tree.find_class("smalltext")[0]
        post_date = ""
        edited = smalltext.find_class("edited")
        if len(edited) > 0:
            post_date = edited[0].text_content()
        else: 
            post_date = html.tostring(smalltext).decode("utf-8")[len('<div class="smalltext">'):-len('</div>')]
        #post_date = post_date.replace('<b>Today</b> at', TODAY)

        try:
            post_date = datetime.strptime(post_date, '%B %d, %Y, %I:%M:%S %p')
        except:
            post_date = None
        return post_date, clean_content
    
    def is_to_keep(self):
        return (self.is_legitimate_content() and self.is_within_time_range())
    
    def is_legitimate_content(self):
        return not (self.post_date is None)
    
    def is_within_time_range(self):
        return (DATE_MIN <= self.post_date < DATE_MAX)
    def is_above_time_range(self):
        return (DATE_MAX <= self.post_date)
    def is_under_time_range(self):
        return (self.post_date < DATE_MIN)
    
    def __str__(self):
        return "{}, {}: \"{}{}\"".format(self.author_id, self.post_date, self.post_content[:40], " [...]" if len(self.post_content)>40 else "")
    def __repr__(self):
        return "{tid};\t{pid};\t{aid};\t{date};\t{content};;".format(
            tid     = self.thread_id,
            pid     = self.post_id,
            aid     = self.author_id,
            date    = self.post_date,
            content = self.post_content)
    def out(self):
        return self.__repr__()

# ==============================================================================

class Thread:
    def __init__(self, board_id=0, url="0", args=None):
        self.board_id = board_id
        self.thread_id = url[url.find('topic=')+len('topic='):-2]
        self.posts = []

        print(self.thread_id, end="")

        nb_pages = getPageCount(url)
        for i in range(nb_pages):
            page = getPage(i, url, 20)
            thread_tree = html.fromstring(page.content)
            page_posts = self._find_posts(thread_tree)

            assert len(page_posts) != 0, \
                "Please restart with 'WebScraper({}, {}, {})'".format(board_id, args['start_board_page'], args['start_thread'])

            page_posts = self._assign_post_ids(20*i, page_posts)

            if len(page_posts) == 0 or page_posts[-1].is_under_time_range():
                continue
            else:
                self.posts += page_posts
                if self.posts[-1].is_above_time_range():
                    break
        
        self.posts += [p for p in self.posts if p.is_to_keep()]
        self.title = self._find_thread(thread_tree)

        if self.is_to_keep():
            saveThreads(board_id, [self.out()])
            savePosts(board_id, self.out_posts())
        print(".", end=" ")

    def is_to_keep(self):
        return (len(self.posts) > 0)
    
    def _assign_post_ids(self, id0=0, posts=None):
        pids = [p for p in posts if p.is_legitimate_content()]
        for p in pids:
            p.thread_id = self.thread_id
            p.post_id = id0
            id0 += 1
        return pids
    
    def _find_thread(self, thread_tree=None):
        title = html.tostring(thread_tree).decode("utf-8")
        title = title[title.find("<title>")+len("<title>"):title.find("</title>")]
        return title
    
    def _find_posts(self, thread_tree=None):
        posterinfos = thread_tree.find_class("poster_info")
        posts = thread_tree.find_class("td_headerandpost")
        min_len = min(len(posterinfos),len(posts))
        return [Post(posterinfos[i],posts[i]) for i in range(min_len)]
        
    def __str__(self):
        return "[{}] {} ({} posts kept)".format(self.thread_id, self.title, len(self.posts))
    def __repr__(self):
        return "{tid};\t{bid};\t{title};;".format(
            tid=self.thread_id,
            bid=self.board_id,
            title=self.title)
    def out(self):
        return self.__repr__()
    def out_posts(self):
        return [p.out() for p in self.posts]

# ==============================================================================

class Board:
    def __init__(self, board_id=0, start_board_page=None, start_thread=0):
        url = "https://bitcointalk.org/index.php?board=" + str(board_id) + ".0"
        
        self.board_id = board_id
        self.threads = []
        nb_threads = 0
        nb_pages = getPageCount(url) if start_board_page is None else start_board_page
        for i in reversed(range(nb_pages)):
            args = { "start_board_page":i, "start_thread":start_thread }
            page = getPage(i, url, 40)
            board_tree = html.fromstring(page.content)
            page_threads = self._find_threads(board_tree, args)
            nb_threads += len([t for t in page_threads if t.is_to_keep()])
            #self.threads += [t for t in page_threads if t.is_to_keep()]
            #self._save_to_file([t for t in page_threads if t.is_to_keep()])

            print("\nBoard {}: page {}/{} done. ({} threads kept)".format(board_id, i+1, nb_pages, nb_threads), end="\n\n\t")
            nb_threads = 0
            start_thread = 0
    
    def _find_threads(self, board_tree=None, args=None):
        matches = re.findall(r"msg_\d+", html.tostring(board_tree).decode("utf-8"))
        threads = []
        for span_id in matches[args['start_thread']:]:
            span = html.tostring(board_tree.get_element_by_id(span_id)[0]).decode("utf-8")
            href = span[span.find('href="')+len('href="'):]
            href = href[:href.find('"')]
            threads += [href]
        return [Thread(self.board_id, 
                       threads[i], 
                       { **args, "start_thread":i }
                    ) for i in range(len(threads))]

    def __str__(self):
        return "[{}] ({} threads kept)".format(self.board_id, len(self.threads))

    def _save_to_file(self, threads):
        if len(threads) > 0:
            saveThreads(self.board_id, [t.out() for t in threads])
            savePosts(self.board_id, [p for t in threads for p in t.out_posts()])


# ==============================================================================
# data_bitcointalk/
def getThreadFilenames(board_id):
    return glob.glob('{}/board{}_threads.txt'.format(PATH, board_id))

def getPostFilenames(board_id):
    return glob.glob('{}/board{}_posts_[0-9][0-9].txt'.format(PATH, board_id))

def getLastThreadFilename(board_id):
    curr = getThreadFilenames(board_id)
    fn = '{}/board{}_threads.txt'.format(PATH, board_id)
    #assert fn in curr, "Last thread's file ({}) does not exist".format(fn)
    return fn

def getLastPostFilename(board_id):
    curr = getPostFilenames(board_id)
    n = ("0" + str(len(curr)))[-2:]
    fn = '{}/board{}_posts_{}.txt'.format(PATH, board_id, n)
    #assert fn in curr, "Last post's file ({}) does not exist".format(fn)
    return fn

def getNextThreadFilename(board_id):
    curr = getThreadFilenames(board_id)
    fn = '{}/board{}_threads.txt'.format(PATH, board_id)
    assert not (fn in curr), "Next thread's file ({}) already exists".format(fn)
    return fn

def getNextPostFilename(board_id):
    curr = getPostFilenames(board_id)
    n = ("0" + str(len(curr)+1))[-2:]
    fn = '{}/board{}_posts_{}.txt'.format(PATH, board_id, n)
    assert not (fn in curr), "Next post's file ({}) already exists".format(fn)
    return fn

MAX_FILE_LEN = 3e8
MAX_FILE_SIZE = MAX_FILE_LEN / 1.1

def freeLength(curr_size):
    return MAX_FILE_LEN - (1.1 * curr_size)

def savePosts(board_id, posts):
    fn = getLastPostFilename(board_id)
    curr_size = os.stat(fn).st_size
    if curr_size >= MAX_FILE_SIZE:
        fn = getNextPostFilename(board_id)
        curr_size = 0

    content = "\n".join(posts) + "\n"
    with open(fn, "a", encoding="utf-8") as fp:
        fp.write(content)

def saveThreads(board_id, threads):
    fn = getLastThreadFilename(board_id)
    curr_size = os.stat(fn).st_size
    if curr_size >= MAX_FILE_SIZE:
        fn = getLastThreadFilename(board_id)
        curr_size = 0

    content = "\n".join(threads) + "\n"
    with open(fn, "a", encoding="utf-8") as fp:
        fp.write(content)

# ==============================================================================

class WebScraper:
    def __init__(self, board_id, start_page=None, start_thread=0):
        print(">>> Scraping Board:{}.{}, {}th Thread...\n".format(board_id, (start_page or 0)*40, start_thread+1))
        self.board = Board(board_id, start_page, start_thread)

# 1.1 byte / character
# 3e5 characters / file
