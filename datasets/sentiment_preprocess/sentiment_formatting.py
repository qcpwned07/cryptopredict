import pandas as pd
import glob

columns1 = ['id','timestamp','polarity','subjectivity']
columns2 = ['timestamp','count','polarity','subjectivity']
BASE PATH = 'E:/data_bitcointalk'

def merge_timestamps():
    df = None
    filenames = sorted(glob.glob('{}/analyzed_board*_posts_*.csv'.format(BASE_PATH)))
    for fn in filenames:
        d2 = pd.read_csv(fn)
        df = df.append(d2) if df is not None else d2
    print(1)
    for index, row in df.iterrows():
        ts = row['timestamp']
        ts -= (ts % 60)
        row['timestamp'] = ts
    print(2)
    for ts, group in df.groupby('timestamp'):
        count = len(group.index)
        polarities = (group['polarity'].sum() / count) if count > 0 else 0
        subjectivities = (group['subjectivity'].sum() / count) if count > 0 else 0
        data += [[ts, count, polarities, subjectivities]]
    print(3)

    df2 = pd.DataFrame(data, columns=columns2)
    print(df2.head())
    df2.to_csv('{}/sentiments.csv'.format(BASE_PATH), index=None, header=True)