from rosette.api import API, DocumentParameters, RosetteException
import glob
import os
from datetime import datetime
import pandas as pd
from textblob import TextBlob
import nltk
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')

POST_COLS = {
    'THREAD_ID': 0,
    'POST_ID':   1,
    'AUTHOR_ID': 2,
    'DATE':      3,
    'CONTENT':   4
}
SENTIMENT_COLS = {
    'POST_ID':            0,
    'TIMESTAMP':          1,
    'ROSETTE_LABEL':      2,
    'ROSETTE_CONFIDENCE': 3
}

def load_api():
    # Create an API instance
    return API(user_key="3a8690466c54b103e716c06b434bac2e", service_url='https://api.rosette.com/rest/v1/')

def rosette_sentiment(api, message=""):
    params = DocumentParameters()
    params["language"] = "eng"
    params.load_document_string(message)
    result = None
    try:
        result = api.sentiment(params)
    except RosetteException as exception:
        print('"{}"'.format(message))
        print(exception)
    return result["document"] if result is not None else result


# api = API(user_key=API_KEY, service_url=alt_url)
# Set selected API options.
# For more information on the functionality of these
# and other available options, see Rosette Features & Functions
# https://developer.rosette.com/features-and-functions#sentiment-analysis

# api.set_option('modelType','dnn') #Valid for English only

def textblob_sentiment(msg):
    analysis = TextBlob(msg)
    return analysis.sentiment

def date_to_timestamp(datestr):
    d = datetime.strptime(datestr, '%Y-%m-%d %H:%M:%S')
    return datetime.timestamp(d)

def analyze_entries():
    filenames = sorted(glob.glob('E:/data_bitcointalk/board*_posts_*.txt'))
    api = load_api()
    for fnIn in filenames:
        ## Parsing
        entries = []
        with open(fnIn, "r", encoding="utf-8") as fp:
            entries = fp.read()
        entries = entries.split(";;\n")
        entries = [e.split(";\t") for e in entries]

        # Formatting & Analyzing
        data = []
        for (tid, pn, _, date, content) in entries:
            pid = tid + '_' + pn
            ts = date_to_timestamp(date)

            sentiment = rosette_sentiment(api, content)
            label = sentiment["label"]
            confidence = sentiment["confidence"]

            data += [[pid, ts, label, confidence]]

        ## Writing
        df = pd.DataFrame(data, columns=['id','timestamp','rosette_label','rosette_confidence'])
        fnOut = "{}analyzed_board{}.csv".format(fn.split('board')[0],fn.split('board')[1][:-4])
        df.to_csv(fnOut, index=None, header=True)

def analyze_entries2():
    filenames = sorted(glob.glob('E:/data_bitcointalk/board*_posts_*.txt'))
    for fnIn in filenames:
        ## Parsing
        entries = []
        with open(fnIn, "r", encoding="utf-8") as fp:
            entries = fp.read()
        entries = entries.split(";;\n")
        entries = [e.split(";\t") for e in entries]

        # Formatting & Analyzing
        data = []
        for (tid, pn, _, date, content) in entries:
            pid = tid + '_' + pn
            ts = date_to_timestamp(date)

            sentiment = textblob_sentiment(content)
            polarity = sentiment.polarity
            subjectivity = sentiment.subjectivity

            data += [[pid, ts, polarity, subjectivity]]

        ## Writing
        df = pd.DataFrame(data, columns=['id','timestamp','polarity','subjectivity'])
        fnOut = "{}analyzed_board{}.csv".format(fnIn.split('board')[0],fnIn.split('board')[1][:-4])
        df.to_csv(fnOut, index=None, header=True)